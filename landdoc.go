package models

import "time"

// Land document
type LandDoc struct {
	ID               string     `json:"id"`
	LandUrn          string     `json:"land_urn"`
	SellerAadhar     string     `json:"seller_aadhar"`
	BuyerAadhar      string     `json:"buyer_aadhar"`
	DocCreatorAadhar string     `json:"doc_creator_aadhar"`
	DocumentValue    int        `json:"doc_val"`
	CreatedAt        *time.Time `json:"created_at"`
	DocHash          string     `json:"doc_hash"`
	DocPath          string     `json:"doc_path"`
	ExpriesAt        *time.Time `json:"expires_at"`
	Status           bool       `json:"status"`
}
